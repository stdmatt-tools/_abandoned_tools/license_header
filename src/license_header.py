#!/usr/bin/env python3
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : license_header.py                                             ##
##  Project   : license_header                                                ##
##  Date      : Feb 23, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt 2020                                                  ##
##                                                                            ##
##  Description :                                                             ##
##    Manages the license header of file!                                     ##
##    Just like this one..                                                    ##
##---------------------------------------------------------------------------~##


##----------------------------------------------------------------------------##
## Constants                                                                  ##
##----------------------------------------------------------------------------##
import os;
import os.path;
import subprocess;
import sys;
import argparse;
import shlex;
import datetime;
import re;
import inspect;


##----------------------------------------------------------------------------##
## Constants                                                                  ##
##----------------------------------------------------------------------------##
PROGRAM_NAME="license-header";
PROJECT_VERSION="1.2.0";
PROJECT_COPYRIGHT_YEARS="2020";

TEMPLATE_HEADER_FILENAME = "/etc/stdmatt/license_header/stdmatt_header.txt";
LOGO_HEADER_FILENAME     = "/etc/stdmatt/license_header/stdmatt_logo.txt";


##----------------------------------------------------------------------------##
## Globals                                                                    ##
##----------------------------------------------------------------------------##
is_debug = False;


##----------------------------------------------------------------------------##
## Types                                                                      ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
class Comment_Info():
    def __init__(self, start, end, first_line, empty_line, last_line):
        self.start      = start;
        self.end        = end;
        self.first_line = first_line;
        self.empty_line = empty_line;
        self.last_line  = last_line;

##------------------------------------------------------------------------------
class Author_Info():
    def __init__(self, name, email):
        self.name  = name;
        self.email = email;

##------------------------------------------------------------------------------
class Copyright_Info():
    def __init__(self, start_year, end_year):
        self.start_year = int(start_year);
        self.end_year   = int(end_year);

        #separator  = ", " if int(end_year) - int(start_year) == 1 else " - ";

##------------------------------------------------------------------------------
class File_Info():
    def __init__(
        self,
        filename,
        project,
        date,
        author,
        copyright,
        description_lines = [],
        last_comment_line = 0
    ):
        self.filename          = filename;
        self.project           = project;
        self.date              = date;
        self.license           = "GPLv3";
        self.author_info       = author;
        self.copyright_info    = copyright;
        self.description_lines = description_lines;
        self.last_comment_line = last_comment_line;


##----------------------------------------------------------------------------##
## Log Functions                                                              ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def log_error(fmt, *args):
    formatted = fmt.format(*args);
    print("[ERROR] ", formatted);

##------------------------------------------------------------------------------
def log_fatal(fmt, *args):
    if(len(args) != 0):
        formatted = fmt.format(*args);
    else:
        formatted = fmt;
    print("[FATAL]", formatted);
    exit(1);

##------------------------------------------------------------------------------
def log_debug(fmt, *args):
    if(not is_debug):
        return;

    if(len(args) != 0):
        formatted = fmt.format(*args);
    else:
        formatted = fmt;

    stack       = inspect.stack()
    caller_name = stack[1].function

    print("[DEBUG]({func}) {0}".format(formatted, func=caller_name));

##------------------------------------------------------------------------------
def log_info(fmt, *args):
    if(len(args) != 0):
        formatted = fmt.format(*args);
    else:
        formatted = fmt;
    print("[INFO] {0}".format(formatted))


##----------------------------------------------------------------------------##
## OS / Path Functions                                                        ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def os_exec(prog, *args):
    args           = " ".join(args);
    cmd            = "{0} {1}".format(prog, args);
    cmd_components = shlex.split(cmd);
    p = subprocess.Popen(
        cmd_components,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    );
    output, errors = p.communicate();
    if(p.returncode):
        log_fatal(
            "Error while executing: ({0})\nReturn Code:({1})\n\n{1}",
            cmd,
            errors.decode('utf-8'),
            p.returncode
        );
    return output, errors;

##------------------------------------------------------------------------------
def normalize_path(path):
    path = path.strip();
    path = os.path.expanduser(path);
    path = os.path.normcase  (path);
    path = os.path.normpath  (path);
    path = os.path.abspath   (path);

    return path;

##------------------------------------------------------------------------------
def read_all_file(filename):
    try:
        lines = [];
        with open(filename) as f:
            lines = f.readlines();
        return lines;
    except Exception as e:
        log_fatal("File not found - Filename: ({0})", filename)


##----------------------------------------------------------------------------##
## Git Functions                                                              ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def git_exec(path, args):
    path           = normalize_path(path);
    output, errors = os_exec("git", "-C", path, args);
    return output.decode('utf-8').splitlines(keepends=False);


##----------------------------------------------------------------------------##
## Helper / Version Functions                                                 ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def get_help_str():
    msg = """Usage:
  {program_name} [--help] [--version]
  {program_name} [--just-header]    <filename> [... filenames]
  {program_name} [--inline]         <filename> [... filenames]
  {program_name} [--project <name>] <filename> [... filenames]

Options:
  *-h --help     : Show this screen.
  *-v --version  : Show program version and copyright.

  --just-header : Print to stdout just the generated header.
  --inline      : Modify the input file prepeding the generated header.

  --project <name> : Override the project name with the given one.

Notes:
  <filename> must always be specified.

  If no flags are given, the default operation is to print the generated
  header and the previous content of the file into the stdout.

  --just-header takes precedence over --inline.

  Options marked with * are exclusive, i.e. the {program_name} will run that
  and exit after the operation.""";

    return msg.format(program_name=PROGRAM_NAME);

##------------------------------------------------------------------------------
def show_help():
    print(get_help_str());
    exit(0);

##------------------------------------------------------------------------------
def show_version():
    msg = """license_header - {version} - stdmatt <stdmatt@pixelwizards.io>
Copyright (c) {copyright_years} - stdmatt
This is a free software (GPLv3) - Share/Hack it
Check http://stdmatt.com for more :)""";

    formatted = msg.format(
        version=PROJECT_VERSION,
        copyright_years=PROJECT_COPYRIGHT_YEARS
    );

    print(formatted);
    exit(0);


##----------------------------------------------------------------------------##
## Command line Args                                                          ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def parse_args():
    parser = argparse.ArgumentParser(
        usage=get_help_str(),
        add_help=False
    );

    ## Help
    parser.add_argument("--help",    dest="help",    action="store_true", default=False);
    parser.add_argument("--version", dest="version", action="store_true", default=False);

    parser.add_argument("--inline",     dest="inline",      action="store_true", default=False);
    parser.add_argument("--just-header",dest="just_header", action="store_true", default=False);

    parser.add_argument("filenames", nargs="*");
    parser.add_argument("--project", dest="project", action="store", default=None);


    return parser.parse_args();


##----------------------------------------------------------------------------##
## Helper Functions                                                           #3
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def nl(l):
    return l.replace("\n", "") + "\n";

##------------------------------------------------------------------------------
def remove_nl(line):
    line = line.replace("\n", "");
    return line;

##------------------------------------------------------------------------------
def check_has_shebang(lines):
    if(len(lines) < 1):
        return False;
    line = lines[0];
    return line.startswith("#!");

##------------------------------------------------------------------------------
def filetype_to_ext(file_type):
    file_type = file_type.lower();

    if(file_type == "c"           ): return "c";
    if(file_type == "c++"         ): return "cpp";
    if(file_type == "posix"       ): return "sh";
    if(file_type == "sh"          ): return "sh";
    if(file_type == "bourne-again"): return "sh";

    return None;

##------------------------------------------------------------------------------
def find_comment_type(filename):
    _, ext = os.path.splitext(filename);

    if(len(ext) == 0):
        output, errors = os_exec("file", filename);
        components     = str(output).split(" ");
        file_type      = components[2];

        ext = filetype_to_ext(file_type);

    if(ext is None):
        log_info(
            "Cound't find a extension of the file: ({0}) - Using shell like comments",
            filename
        );
        ext = "sh";

    ext = ext.lstrip(".");
    extensions = {
        "c"    : ["/*"   , "*/"  ],
        "h"    : ["/*"   , "*/"  ],
        "cpp"  : ["//"   , "//"  ],
        "hpp"  : ["//"   , "//"  ],
        "js"   : ["//"   , "//"  ],
        "py"   : ["##"   , "##"  ],
        "sh"   : ["##"   , "##"  ],
        "html" : ["<!-- ", " -->"],
        "css"  : ["/*"   , "*/"  ],
    };
    comments = extensions.get(ext);
    if(comments is None):
        log_info(
            "Couldn't find comment type for extension ({0}) - Using shell like comments",
           ext
        );
        comments = extensions.get("sh");

    return comments;


##----------------------------------------------------------------------------##
## Format / Parse                                                             ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def format_header_date(date):
    return date.strftime("%b %d, %Y");

##------------------------------------------------------------------------------
def try_parse_header_date(value):
    if(value is None):
        return None;

    ## @TODO(stdmatt): Add more formats...
    formats = ["%b %d, %Y"];
    for format in formats:
        log_debug("Trying to parse date with format: ({0})", format);
        parsed = datetime.datetime.strptime(value, format);
        if(parsed is not None):
            return parsed

    log_debug("Failed to parse date with all formats");
    return parsed;

##------------------------------------------------------------------------------
def format_header_author(author_info):
    return "{0} <{1}>".format(author_info.name, author_info.email);

##------------------------------------------------------------------------------
def try_parse_header_author(value):
    result = re.match(r"(.*) (.*)", value);
    if(result is None):
        return None;

    g1 = result.group(1);
    g2 = result.group(2);

    if(len(g1) == 0):
        return None;

    author_info = Author_Info(
        g1.strip(" ").strip("-").strip(" "),
        g2.strip(" ")
    );
    return author_info;

##------------------------------------------------------------------------------
def format_header_copyright(author_info, copyright_info):
    diff = int(copyright_info.end_year) - int(copyright_info.start_year);
    if(diff == 0):
        return "{0} - {1}".format(author_info.name, copyright_info.end_year);
    if(diff == 1):
        return "{0} - {1}, {2}".format(
            author_info.name,
            copyright_info.start_year,
            copyright_info.end_year
        );

    return "{0} - {1} - {2}".format(
        author_info.name,
        copyright_info.start_year,
        copyright_info.end_year
    );

##------------------------------------------------------------------------------
def try_parse_header_copyright(value):
    ## @todo(stdmatt): We should work to get all the authors/years information
    ## today we are just getting the first year...
    result = re.match(r"([a-z]|[A-z]*)[ *| *\- *](.*)", value);
    if(result is None):
        log_debug("Couldn't extract copyright from value: {0}", value);
        return None;

    group1 = result.group(1);
    group2 = result.group(2);
    log_debug("Extracted two groups: ({0}) and ({1})", group1, group2);

    if(len(group1) == 0 or len(group2) == 0):
        log_debug("Neigher groups can be empty... ({0}) and ({1})", len(group1), len(group2));
        return None;

    min_year = 9999;
    max_year = 0;
    for comp in group2.replace(",", " ").replace("-", " ").split(" "):
        try:
            year = int(comp);
            min_year = min(min_year, year);
            max_year = max(max_year, year);
        except:
            continue;

    log_debug("Extracted copyright years - Start: ({0}) - End ({1})", min_year, max_year);
    copyright_info = Copyright_Info(min_year, max_year);
    return copyright_info;


##------------------------------------------------------------------------------
def insert_comments(comment_info, logo_lines, template_lines, file_info):
    ##
    ## Bake the file information into the template lines.
    baked_lines = [];
    for line in template_lines:
        line  = line.replace("\n", "");
        index = line.find("$");
        if(index == -1):
            baked_lines.append(line);
            continue;

        ## @notice(stdamtt): We're assuming that there's nothing more
        ## after the key itself...
        key       = line[index:];
        clean_key = key[1:].lower();

        value = getattr(file_info, clean_key, None);
        if(clean_key == "date"):
            value = format_header_date(file_info.date);
        elif(clean_key == "author"):
            value = format_header_author(file_info.author_info);
        elif(clean_key == "copyright"):
            value = format_header_copyright(
                file_info.author_info,
                file_info.copyright_info
            );
        elif(clean_key == "description"):
            value = "";

        line = "  {0}".format(line.replace(key, value));
        baked_lines.append(line);

    for desc_line in file_info.description_lines:
        baked_lines.append(desc_line);

    ##
    ## Insert the appropriated comments.
    commented_lines = [];
    def comment_line(line):
        spaces_count = (80 - (len(comment_info.start)+len(line)+len(comment_info.end)));
        spaces       = " " * spaces_count;

        line = "{begin}{content}{spacing}{end}".format(
            begin   = comment_info.start,
            content = line,
            spacing = spaces,
            end     = comment_info.end
        );

        return line;

    for line in logo_lines:
        commented_lines.append(comment_line(remove_nl(line)));

    commented_lines.append(comment_line(""));

    for line in baked_lines:
        commented_lines.append(comment_line(line));

    commented_lines.insert(0, comment_info.first_line);
    commented_lines.append(comment_info.empty_line);
    commented_lines.append(comment_info.last_line );

    return commented_lines;

##------------------------------------------------------------------------------
def get_comment_info(filename):
    start, end = find_comment_type(filename);
    def build_line(prefix, middle, sufix):
            start_end_len    = len(start ) + len(end  );
            prefix_sufix_len = len(prefix) + len(sufix);
            space_count      = (80 - start_end_len - prefix_sufix_len);
            line = start + prefix       \
                 + middle * space_count \
                 + sufix + end;
            return line;

    first_line = build_line("~", "-", "-");
    empty_line = build_line(" ", " ", " ");
    last_line  = build_line("-", "-", "~");

    comment_info = Comment_Info(
        start,
        end,
        first_line,
        empty_line,
        last_line,
    );

    return comment_info;

##------------------------------------------------------------------------------
def get_file_info(filename):
    dir_path  = os.path.dirname(filename);

    ##
    ## File.
    _filename = os.path.basename(filename);
    log_debug("Filename: {0}", _filename)


    ##
    ## Project.
    ## The git output is something like:
    ##    git remote -v
    ##    origin	https://gitlab.com/stdmatt-libs/shellscript_utils.git (fetch)
    ##    origin	https://gitlab.com/stdmatt-libs/shellscript_utils.git (push)
    ## We are only interested on the first line and the basename of the remote
    ## basename of the remote that we are fetching from.
    result        = git_exec(dir_path, "remote -v");
    _project_name = "";

    ## If we don't have the fetch url, try to find where's the git directory.
    if(len(result) == 0):
        log_debug("Couldn't find git remote for repository - Trying to get the git root directory.");
        result        = git_exec(dir_path, "rev-parse --show-toplevel");
        _project_name = result[0];
    else:
        log_debug("Found remote for repository - Getting the project name from it.");
        fetch_url     = result[0].split()[1];
        _project_name = os.path.splitext(fetch_url)[0];

    _project_name = os.path.basename(_project_name);
    log_debug("Project Name: {0}", _project_name)

    ##
    ## Date.
    result = git_exec(
        dir_path,
        "log --follow --format=%at --reverse -- {0}".format(filename)
    );
    _file_dt = None;

    ## If we don't have the git date for the file, try to find
    ## the it's date using the os.stat functions
    if(len(result) == 0):
        log_debug("Couldn't find file date from git - Trying to get from stat(2).");
        stat_result = os.stat(filename);
        if(getattr(stat_result, "st_birthtime", False)):
            log_debug("stat_result has atribute st_birthtime");
            _file_dt = stat_result.st_birthtime;
        else:
            log_debug("stat_result has not atribute st_birthtime - Using st_ctime");
            _file_dt = stat_result.st_ctime;
        _file_dt = datetime.datetime.utcfromtimestamp(int(_file_dt));
    else:
        log_debug("Found file date from git.");
        git_utc_dt = datetime.datetime.utcfromtimestamp(int(result[0]));
        _file_dt   = git_utc_dt

    log_debug("File date: {0}", _file_dt)

    ##
    ## Author.
    _author_name  = git_exec(dir_path, "config user.name") [0];
    _author_email = git_exec(dir_path, "config user.email")[0];

    log_debug("Author Name: {0}",  _author_name);
    log_debug("Author Email: {0}", _author_email);

    _author_info = Author_Info(_author_name, _author_email);

    ##
    ## Copyright.
    _copyright_start = _file_dt.strftime("%Y");
    _copyright_end   = datetime.datetime.utcnow().strftime("%Y")
    _copyright_info = Copyright_Info(_copyright_start, _copyright_end)

    log_debug("Copyright Year: {0} - {1}",  _copyright_start, _copyright_end);

    ##
    ## File Info
    file_info = File_Info(
        _filename,
        _project_name,
        _file_dt,
        _author_info,
        _copyright_info
    );

    return file_info;

##------------------------------------------------------------------------------
def extract_file_info(file_lines, comment_info):
    ## Not enough lines...
    if(len(file_lines) <= 2):
        return None;
    ## Doesn't start with the our start comment...
    if(remove_nl(file_lines[0]) != comment_info.first_line):
        return None;

    ## @todo(stdmatt): We can just use this instead of the template file....
    header_keys = [
        "File      :",
        "Project   :",
        "Date      :",
        "License   :",
        "Author    :",
        "Copyright :",
        "Description :"
    ];
    extracted_dict = {};

    ##
    ## Extract the information from the header...
    extracting_description = False;
    last_comment_line      = 0;
    for line_no, line in enumerate(file_lines[1:]):
        line = remove_nl(line);

        if(line == comment_info.last_line):
            last_comment_line = (line_no + 1)
            log_debug("Found the end of header comment at line: ({0})", last_comment_line);
            break;

        ## Non comment line... We don't know how to handle this so
        ## the safest way is to make as we couldn't file the existing header.
        if(not line.startswith(comment_info.start) \
        or not line.endswith  (comment_info.end)):
            log_error("Found a non comment line at line: ({0}) - Line: {1}", line_no, line);
            return None;

        clean_line = line.replace(comment_info.start, "") \
                         .replace(comment_info.end,   "") \
                         .strip(" ");

        if(not extracting_description):
            for key in header_keys:
                if(not clean_line.startswith(key)):
                    continue;

                log_debug("Found line with info key: line_no: ({0}), {1}", line_no, line);
                clean_key = key.lower().replace(":", "").strip();
                value     = clean_line .replace(key, "").strip();

                log_debug("Extracted info - Key: ({0}) - Value: ({1})", clean_key, value);
                extracted_dict[clean_key] = value;

                if(clean_key == "description"):
                    log_debug("Found description key");
                    extracting_description    = True;
                    extracted_dict[clean_key] = [];
        else:
            ## @notice(stdmatt): Almost like the clean line, but we need to
            ## keep the formating of the description instead...
            desc_line = line.replace(comment_info.start, "") \
                            .replace(comment_info.end,   "");

            log_debug("Extracting description:\n{0}", desc_line);
            extracted_dict["description"].append(desc_line);


    ##
    ## Create a file info with the extracted information...
    filename          = extracted_dict.get("file",    None);
    project           = extracted_dict.get("project", None);
    date              = try_parse_header_date     (extracted_dict.get("date",      None));
    author            = try_parse_header_author   (extracted_dict.get("author",    None));
    copyright         = try_parse_header_copyright(extracted_dict.get("copyright", None));
    description_lines = extracted_dict.get("description", []);

    file_info = File_Info(
        filename,
        project,
        date,
        author,
        copyright,
        description_lines,
        last_comment_line
    );

    return file_info;

##------------------------------------------------------------------------------
def merge_file_info(file_new_info, file_old_info):
    if(file_new_info is None):
        log_debug("New info is empty - Using the old info");
        return file_old_info;
    if(file_old_info is None):
        log_debug("Old info is empty - Using the new info");
        return file_new_info;

    merged_copyright = Copyright_Info(
        min(file_new_info.copyright_info.start_year, file_old_info.copyright_info.start_year),
        max(file_new_info.copyright_info.end_year,   file_old_info.copyright_info.end_year)
    )

    is_empty_description = True;
    for desc_line in file_old_info.description_lines:
        if(len(remove_nl(desc_line).replace(" ", "")) != 0):
            is_empty_description = False;
            break;

    merged_description_lines = []
    if(not is_empty_description):
        merged_description_lines = file_old_info.description_lines;

    merged_file_info = File_Info(
        file_new_info.filename,
        file_new_info.project,
        file_new_info.date,
        file_new_info.author_info,
        merged_copyright,
        merged_description_lines
    );

    return merged_file_info;


##------------------------------------------------------------------------------
def print_output(shebang_line, lines, buffer):

    if(len(shebang_line) != 0):
        buffer.write(nl(shebang_line));

    for line in lines:
        buffer.write(nl(line));

##----------------------------------------------------------------------------##
## Entry Point                                                                ##
##----------------------------------------------------------------------------##
def run():
    ## Parse the command line arguments.
    args = parse_args();
    if(args.help):
        show_help();
    if(args.version):
        show_version();

    if(args.filenames is None or len(args.filenames) == 0):
        log_fatal("Missing filenames");

    modify_inline = args.inline;
    just_header   = args.just_header;
    project_name  = args.project;


    for filename in args.filenames:
        filename = normalize_path(filename);
        if(not os.path.isfile(filename)):
            log_fatal("No file with filename: ({0})", filename);

        ## Read the files.
        logo_lines     = read_all_file(LOGO_HEADER_FILENAME    );
        template_lines = read_all_file(TEMPLATE_HEADER_FILENAME);
        file_lines     = read_all_file(filename);

        ## Get the shebang line.
        ##   We need that because shebang always need to be 1st.
        shebang_line = "";
        if(check_has_shebang(file_lines)):
            shebang_line = file_lines[0];
            file_lines   = file_lines[1:];

        ## Build the information.
        comment_info  = get_comment_info (filename);
        file_new_info = get_file_info    (filename);
        if(project_name is not None and len(project_name) != 0):
            file_new_info.project = project_name;

        file_old_info   = extract_file_info(file_lines,    comment_info);
        file_final_info = merge_file_info  (file_new_info, file_old_info);

        ## Build the header lines...
        final_lines = insert_comments(
            comment_info,
            logo_lines,
            template_lines,
            file_final_info
        );

        if(just_header):
            print_output("", final_lines, sys.stdout);
            continue;

        ## Remove the old header...
        eat_index = 0;
        if(file_old_info is not None):
            eat_index = (file_old_info.last_comment_line + 1);

        ## Eat the spaces, so always we will have just a new line .
        for i in range(eat_index, len(file_lines)):
            line       = file_lines[i];
            clean_line = remove_nl(line).replace(" ", "");
            if(len(clean_line) == 0):
                eat_index += 1;
            else:
                break;

        file_lines = file_lines[eat_index:];
        output_lines = final_lines + [""] + file_lines;

        if(not modify_inline):
            print_output(shebang_line, output_lines, sys.stdout);
        else:
            with open(filename, "w") as f:
                print_output(shebang_line, output_lines, f);

if __name__ == "__main__":
    run()
